import { isContext } from "vm";

class Life {
    LIVE = "live";
    DEAD = "dead";
    currentGen: Map<number,string>;
    width: number
    height: number
    scale: number

    constructor(x: number, y: number, scale: number) {
        console.log("constructing life.. ")
        this.scale = scale;
        this.width = Math.floor(x/scale);
        this.height = Math.floor(y/scale);
    }
    xyToIndex(x: number, y: number) {
        return y * this.width + x
    }

    indexToXY(i: number) {
        return {x: i%this.height, y: Math.round(i/this.width)}
    }

    deleteCell(x: number, y: number, grid: Map<number, string>) {
        grid.set(this.xyToIndex(x,y),this.DEAD); 
        if (this.countNeighbours(x,y,grid)==0) {
            grid.delete(this.xyToIndex(x,y))
        }
    }
    createCell(x: number, y:number, grid: Map<number, string>) {
        grid.set(this.xyToIndex(x,y),this.LIVE) // create cell and then put surrounding cells into grid
        console.log(`creating cell at ${x},${y}`)
        for (let dx=-1;dx<2;dx++) {
            for (let dy=-1;dy<2;dy++) {
                if (dx != 0 || dy !=0) { // if they are both zero, this is the current cell.. ignore
                    let idx = this.xyToIndex(Math.abs((x+dx)%this.width),Math.abs((y+dy)%this.height))
                    if (!grid.has(idx)) {   // if already in grid, leave it.
                        grid.set(idx,this.DEAD)
                    }
                }
            }
        }
    }
    countNeighbours(x: number, y:number, grid: Map<number, string>) {
        let count=0;
        for (let dx=-1;dx<2;dx++) {
            for (let dy=-1;dy<2;dy++) {
                if (dx != 0 || dy !=0) {
                    let idx = this.xyToIndex(Math.abs((x+dx)%this.width),Math.abs((y+dy)%this.height))
//                    console.log(`assessing neighbour at ${Math.abs((x+dx)%this.width)},${Math.abs((y+dy)%this.height)} - indx ${idx}`)
                    if (grid.get(idx) == this.LIVE) 
                        count ++
                        //console.log('got one..')
                        
                }
            }
        }
        //console.log(`found ${count} neighbours`)
        return count
    }
    calculateNextGen() {
        let grid = this.currentGen;
        let nextGen = new Map<number, string>();

        for(let k of grid.keys()) {
            let i = this.indexToXY(k)
            let c = this.countNeighbours(i.x, i.y, grid)
            // Any live cell with fewer than two live neighbours dies 
            // Any live cell with more than three live neighbours dies 
            // Any live cell with two or three live neighbours lives
            // Any dead cell with exactly three live neighbours will come to life.
            //copy only cells that live, above.
            if (c==2 && grid.get(k)==this.LIVE) {  // we are alive with two neighbours, we live
                this.createCell(i.x,i.y,nextGen)
                console.log("Live cell has two neighbours..")
            }
            if (c==3) {
                console.log("Live or dead cell has three neighbours..")
                this.createCell(i.x,i.y,nextGen);  // any cell with three neighbours lives
            }
        };
        this.currentGen = nextGen;
    } 
    drawGridOnCanvasContext(ctxt: CanvasRenderingContext2D) {
       
        ctxt.clearRect(0,0, (this.width+1)*this.scale, (this.height+1)*this.scale)
        ctxt.strokeStyle = 'black';
        for(let k of this.currentGen.keys()) {
            if (this.currentGen.get(k) == this.LIVE) {
                let i = this.indexToXY(k);
                ctxt.fillRect(i.x*this.scale, i.y*this.scale,this.scale, this.scale);
            }
        }

    }
    init() {
        this.currentGen = new Map<number,string>();
        this.insertRandomCells(2000)
        this.insertOscillator(100,100);
        this.insertGlider(20,20)
        this.insertGlider(20,60)
        this.insertGlider(120,10)
    }
    insertRandomCells(cnt: number) {
        for (let i=0; i<cnt; i++) {
            let x = Math.floor(Math.random() * this.width)
            let y = Math.floor(Math.random() * this.height)
            this.createCell(x,y,this.currentGen)
        }
    }
    insertOscillator(x:number, y: number) {
        // only one way for now. 
        for (let i=0; i<3; i++) {
            let xn = Math.abs((x+i)%this.width)
            this.createCell(xn,y, this.currentGen);
        }
        console.log(`size of grid now.. ${this.currentGen.size}`)
        console.log(`Number of neighbours of 101,100.. ${this.countNeighbours(101,100,this.currentGen)}`)
        this.dumpGrid();
    }
    insertGlider(x:number, y: number) {
        this.createCell(x,y, this.currentGen);
        this.createCell(x+2,y, this.currentGen);
        this.createCell(x+1,y+1, this.currentGen);
        this.createCell(x+2,y+1, this.currentGen);
        this.createCell(x+1,y+2, this.currentGen);
    }
    dumpGrid() {
        console.log('Dumping Grid..');
        for(let k of this.currentGen.keys()) {
            let yesNo = this.currentGen.get(k)
            let i = this.indexToXY(k);
            console.log(`Idx - ${k}, (x,y) - (${i.x},${i.y}) - Value ${yesNo} - neighbour count-> ${this.countNeighbours(i.x,i.y,this.currentGen)}`)

        }
        console.log("testing index conversion")
        this.testIdxConversion()
    }
    testIdxConversion() {
        let x = 24;
        let y = 76;
        let idx = this.xyToIndex(x,y);
        let i = this.indexToXY(idx);
        console.log(`Index of (${x},${y}) is .. ${idx}`)
        console.log(`Conversion back to x and y is (${i.x},${i.y})`)
    }
     

}


/**  approach.

we have a map of indices to values, where a value is boolean.  the index is a combination of simple 
(y * width + x) and True or False.

After a generation of cells are in the grid, do the following.
Cycle through all cells. 
    for each cell
    if cell is live, check its neighbours, if they are not in the grid, add them.
    If the cell is dead. Check it's neighbours, if they are all dead, remove them.
*/

class Canvas {
    private canvas: HTMLCanvasElement;

    private context: CanvasRenderingContext2D;
    private paint: boolean;
    private life: Life;
    private x: number;
    private scale: number;
    private delay: number = 100;
    private timeStamp: number = 0;

    constructor() {
        console.log("Constructing..")
        this.x = 0
        this.canvas = document.getElementById('canvas') as
                 HTMLCanvasElement;
        console.log(`canvas =${this.canvas}`)
        this.context = this.canvas.getContext("2d");
        console.log(`context = ${this.context}`)
        this.context.lineWidth = 1;
        this.scale = 3;
    }
    start() {
        console.log("Starting..")
        this.life = new Life(this.canvas.width, this.canvas.height, this.scale)
        this.life.init()
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw()
    }
    test() {
        console.log("Starting test..")
        this.life = new Life(this.canvas.width, this.canvas.height, this.scale)
        this.life.init()
        this.life.drawGridOnCanvasContext(this.context);
        this.life.calculateNextGen();
        this.life.dumpGrid();

    }
    private redraw() {
        
        let now = Date.now();
        if (now-this.timeStamp > this.delay) {
            this.timeStamp = now;
            this.life.drawGridOnCanvasContext(this.context);
            this.life.calculateNextGen();
        }
        window.requestAnimationFrame(this.redraw.bind(this));
    }
    
}
new Canvas().start();
//new Canvas().test();

